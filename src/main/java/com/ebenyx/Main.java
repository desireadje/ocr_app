package com.ebenyx;
import com.ebenyx.jpa.TransactionAutoriseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.aspose.cells.*;
import org.apache.commons.io.FileUtils;
import com.spire.pdf.FileFormat;
import com.spire.pdf.graphics.PdfFont;
import com.spire.pdf.graphics.PdfFontFamily;
import com.spire.pdf.graphics.fonts.PdfUsedFont;
import com.spire.pdf.conversion.XlsxLineLayoutOptions;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;


/**
 *
 * @author desireadje
 */
@EnableAsync
// @EnableScheduling
@SpringBootApplication
public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static String PID;
    private static String PDF_EXT = "pdf";
    private static String XLSX_EXT = "xlsx";

    @Autowired private Environment env;

    // @Autowired private transient Job job;

    @Autowired public static TransactionAutoriseRepository repository;

    public Main(TransactionAutoriseRepository repository) {
        this.repository = repository;
    }

    public static void main(String[] args) throws Exception {
        readPdf();
        // deletingBlankColumns();

        // changeFont();
        // readOnePdf();
        // _deletingBlankColumns();
        runSpringApplication(args);
        // initDatas();
    }

    // @Async
    public static void initDatas() throws FileNotFoundException {
        logger.info("Run initDatas");
        // PoijiTrans poijiTrans = new PoijiTrans();
        // poijiTrans.main(repository);
        // poijiTrans.listener();
    }

    public static void runSpringApplication(String[] args){
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        if (args.length>0) {
            if ("service".equals(args[0])) {
                runAsService();
                return;
            }
            logger.info("arguments  length " + args.length);
            killApp(args[0]);
        }
        SpringApplication springApplication = new SpringApplication(Main.class);
        springApplication.addListeners(new ApplicationPidFileWriter());
        springApplication.run(args);
        setupProcessId();
    }

    public static void runAsService(){
        try {
            String cmd = "java -jar ./target/sgci_avis_debit_credit-1.0.jar";
            System.out.println(cmd);
            Runtime.getRuntime().exec(cmd);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void setupProcessId(){
        try {
            File file = new File("./application.pid");
            BufferedReader br = new BufferedReader(new FileReader(file));
            PID = br.readLine();
            br.close();
            logger.info("PID {}",PID);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void killApp(String pid){
        try {
            String cmd = String.format("kill -9 %s", pid);
            logger.info(cmd);
            Runtime.getRuntime().exec(cmd);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void readPdf(){
        try{
            // cleanDirectory
            FileUtils.cleanDirectory(new File("pdf/out/tmp"));

            logger.info("_Run readPdf_");
            Locale newLocale = Locale.ROOT;
            Locale.setDefault(newLocale);

            ArrayList<File> pdfFileList = (ArrayList<File>) displayDirectoryContents(new File("pdf"),PDF_EXT);
            if (pdfFileList.isEmpty()){
                logger.info("Aucun fichier pdf trouvé");
                return;
            }

            for (File pdfFile: pdfFileList) {
                logger.info("Traitement du fichier {}",pdfFile.getName());

                // Créer un objet PdfDocument
                com.spire.pdf.PdfDocument pdfDocument = new com.spire.pdf.PdfDocument();

                // Charger un exemple de fichier PDF
                pdfDocument.loadFromFile(pdfFile.getAbsolutePath());
                // pdfDocument.loadFromFile("pdf/0028894  W_2020.pdf");

                // Définir les options de conversion PDF vers XLSX : rendu de plusieurs pages sur une seule feuille de calcul
                pdfDocument.getConvertOptions().setPdfToXlsxOptions(new XlsxLineLayoutOptions(true,true,true));

                // Enregistrer dans Excel
                // pdfDocument.saveToFile("pdf/traites/0028894  W_2020.xlsx", FileFormat.XLSX);
                String excelFileName = String.format("pdf/traites/%s_.xlsx",FilenameUtils.removeExtension(pdfFile.getName()));
                pdfDocument.saveToFile(excelFileName, FileFormat.XLSX);

                pdfDocument.close();

                logger.info("Convert PDF to Excel Done : {}", excelFileName);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public static void deletingBlankColumns() throws Exception{
        logger.info("_Run deletingBlankColumns_");

        ArrayList<File> pdfFileList = (ArrayList<File>) displayDirectoryContents(new File("pdf/traites"),XLSX_EXT);
        if (pdfFileList.isEmpty()){
            logger.info("Aucun fichier pdf trouvé");
            return;
        }

        for (File pdfFile: pdfFileList) {
            logger.info("Traitement du fichier {}",pdfFile.getName());

            com.aspose.cells.Workbook workbook = new com.aspose.cells.Workbook(pdfFile.getAbsolutePath());
            com.aspose.cells.WorksheetCollection sheets = workbook.getWorksheets();

            com.aspose.cells.Worksheet sheet0 = sheets.get(0);
            com.aspose.cells.Worksheet sheet1 = sheets.get(1);

            _deletingBlankColumns(workbook, sheet0, sheet1, pdfFile);
        }

        /*
        File pdfFile = new File("pdf/traites/0028894  W_2020.xlsx");
        com.aspose.cells.Workbook workbook = new com.aspose.cells.Workbook(pdfFile.getAbsolutePath());
        com.aspose.cells.WorksheetCollection sheets = workbook.getWorksheets();
        com.aspose.cells.Worksheet sheet0 = sheets.get(0);
        com.aspose.cells.Worksheet sheet1 = sheets.get(1);
        _deletingBlankColumns(workbook, sheet0, sheet1, pdfFile);
         */
    }

    public static void _deletingBlankColumns(com.aspose.cells.Workbook workbook,
                                             com.aspose.cells.Worksheet sheet0,
                                             com.aspose.cells.Worksheet sheet1,
                                             File pdfFile) throws Exception {
        com.aspose.cells.Workbook workbookTo = new com.aspose.cells.Workbook();

        Cell c = null;
        boolean start = false;

        int startI = -1, startJ = -1,style,r;
        for (int i = 0; i < sheet0.getCells().getMaxColumn(); i++) {
            for (int j = 0; j < sheet0.getCells().getMaxColumn(); j++) {
                c = sheet0.getCells().get(i,j);
                if("REF".equalsIgnoreCase((c.getValue()+"").trim()) || "Réf.".equalsIgnoreCase((c.getValue()+"").trim())){
                    startI =i;
                    startJ =j;
                    break;
                }
            }
            if(startI>-1){
                break;
            }
        }

        List<Integer> columnIndex = new ArrayList<>();
        for (int j = startJ; j < sheet0.getCells().getMaxColumn(); j++){
            columnIndex.add(0);
        }

        for (int i = startI; i < sheet0.getCells().getMaxRow(); i++){
            for (int j = startJ; j < sheet0.getCells().getMaxColumn(); j++){
                System.out.println("row "+i);
                c = sheet0.getCells().get(i,j);
                style = c.getStyle(true).getBorders().getByBorderType(BorderType.LEFT_BORDER).getLineStyle();
                r=Math.max(
                        columnIndex.get(j-startJ),
                        style
                );
                columnIndex.set(j-startJ,r);
                System.out.print(style+" ");
            }
            System.out.println();
        }

        for (int j:columnIndex ){
            System.out.print(j+" ");
        }


        // workbook.getWorksheets().add();
        // Worksheet sheetsTo = workbookTo.getWorksheets().get(0);
        Worksheet sheetsTo = workbookTo.getWorksheets().insert(0,SheetType.CHART,"sheet 1");
        sheetsTo.getCells().insertRows(0,sheet0.getCells().getMaxRow()-startI);
        sheetsTo.getCells().insertColumns(0,columnIndex.size());


        //System.out.println("sheet.getCells().getMaxColumn() "+sheet.getCells().getMaxColumn());
        //System.out.println("columnIndex "+columnIndex.size());
        //System.out.println("startJ "+startJ);


        int currentCol;
        String val;
        for (int i = startI; i < sheet0.getCells().getMaxRow(); i++){
            currentCol = 0;
            val = "";
            for (int j = startJ; j < sheet0.getCells().getMaxColumn(); j++){
                if(j>0 && columnIndex.get(j-startJ)>0){
                    sheetsTo.getCells().get(i-startI,currentCol).setValue(val);
                    currentCol++;
                    val = "";
                }
                c = sheet0.getCells().get(i,j);
                val = val+c.getStringValue();
                // System.out.print(c.getValue());
            }
            System.out.println();
        }











        ///////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////
        Cell _c = null;

        boolean _start = false;
        int _startI = -1, _startJ = -1,_style,_r;
        for (int i = 0; i < sheet1.getCells().getMaxColumn(); i++) {
            for (int j = 0; j < sheet1.getCells().getMaxColumn(); j++) {
                _c = sheet1.getCells().get(i,j);
                if("REF".equalsIgnoreCase((_c.getValue()+"").trim()) || "Réf.".equalsIgnoreCase((_c.getValue()+"").trim())){
                    _startI =i;
                    _startJ =j;
                    break;
                }
            }
            if(_startI>-1){
                break;
            }
        }

        List<Integer> _columnIndex = new ArrayList<>();
        for (int j = _startJ; j < sheet1.getCells().getMaxColumn(); j++){
            _columnIndex.add(0);
        }

        for (int i = _startI; i < sheet1.getCells().getMaxRow(); i++){
            for (int j = _startJ; j < sheet1.getCells().getMaxColumn(); j++){
                System.out.println("row "+i);
                _c = sheet1.getCells().get(i,j);
                _style = _c.getStyle(true).getBorders().getByBorderType(BorderType.LEFT_BORDER).getLineStyle();
                _r=Math.max(
                        _columnIndex.get(j-_startJ),
                        _style
                );
                _columnIndex.set(j-_startJ,_r);
                System.out.print(_style+" ");
            }
            System.out.println();
        }

        for (int j:_columnIndex ) {
            System.out.print(j+" ");
        }


        // workbook.getWorksheets().add();
        // Worksheet _sheetsTo = workbookTo.getWorksheets().get(1);
        Worksheet _sheetsTo = workbookTo.getWorksheets().insert(1, SheetType.CHART, "sheet 2");
        _sheetsTo.getCells().insertRows(0,sheet1.getCells().getMaxRow()-_startI);
        _sheetsTo.getCells().insertColumns(0,_columnIndex.size());


        //System.out.println("sheet.getCells().getMaxColumn() "+sheet.getCells().getMaxColumn());
        //System.out.println("columnIndex "+columnIndex.size());
        //System.out.println("startJ "+startJ);


        int _currentCol;
        String _val;
        for (int i = _startI; i < sheet1.getCells().getMaxRow(); i++){
            _currentCol = 0;
            _val = "";
            for (int j = _startJ; j < sheet1.getCells().getMaxColumn(); j++){
                if(j>0 && _columnIndex.get(j-_startJ)>0){
                    _sheetsTo.getCells().get(i-_startI,_currentCol).setValue(_val);
                    _currentCol++;
                    _val = "";
                }
                _c = sheet1.getCells().get(i,j);
                _val = _val+_c.getStringValue();
            }
            System.out.println();
        }


        String excelFileName = String.format("pdf/out/%s.xlsx",FilenameUtils.removeExtension(pdfFile.getName()));
        workbookTo.save(excelFileName);

        logger.info("Deleting Blank Columns Done : {}", excelFileName);
    }






























































    public static void createFolder(String pathString){
        File dir = new File(pathString);
        if (!dir.exists()){
            dir.mkdirs();
        }
        logger.info("Path created {} ",Paths.get(pathString));
    }

    public static void readAllRows() throws Exception {

        com.aspose.cells.Workbook wb = new com.aspose.cells.Workbook("pdf/traites/0028894  W_2020.xlsx");
        //Get the first worksheet.
        com.aspose.cells.WorksheetCollection sheets = wb.getWorksheets();
        com.aspose.cells.Worksheet sheet = sheets.get(0);

        Cells cells = sheet.getCells();
        Range range = cells.getMaxDisplayRange();
        int totalColumns = range.getColumnCount();
        int totalRows = range.getRowCount();
        RowCollection rows = cells.getRows();


        for (int i = 1; i < rows.getCount(); i++) {
            String s = "";
            for (int j = 0; j < totalColumns; j++) {
                // System.out.print(cells.get(i, j).getValue() + "\t");
                s =s+""+cells.get(i, j).getValue()+"\t";
            }

            System.out.println("ligne " + i+ "  : " + s );
            System.out.println("");
        }
    }

    public static void deleteDirectory(File directory) throws IOException {
        FileUtils.cleanDirectory(directory);
    }

    private static void readOnePdf(){
        try{
            logger.info("_Run readOnePdf_");
            Locale newLocale = Locale.ROOT;
            Locale.setDefault(newLocale);

            // Créer un objet PdfDocument
            com.spire.pdf.PdfDocument pdfDocument = new com.spire.pdf.PdfDocument();

            // Charger un exemple de fichier PDF
            File pdfFile = new File("pdf/_2021.pdf");
            pdfDocument.loadFromFile(pdfFile.getAbsolutePath());

            /**
             * @URL : https://www.e-iceblue.com/Tutorials/Spire.PDF/Spire.PDF-Program-Guide/Text/How-to-replace-fonts-in-PDF-document.html
             */
            /*
            // Utilisez l'attribut UsedFonts de la classe PdfDocument pour obtenir toutes les polices utilisées dans le document.
            PdfUsedFont[] fonts = pdfDocument.getUsedFonts();

            PdfFont newfont = new PdfFont(PdfFontFamily.Times_Roman, 11f);
            for (PdfUsedFont font: fonts) {
                font.replace(newfont); // replace font
            }
            pdfDocument.saveToFile(pdfFile.getAbsolutePath());
            */

            // Définir les options de conversion PDF vers XLSX : rendu de plusieurs pages sur une seule feuille de calcul
            pdfDocument.getConvertOptions().setPdfToXlsxOptions(new XlsxLineLayoutOptions(true,true,false));

            // Enregistrer dans Excel
            // pdfDocument.saveToFile("pdf/traites/0028894  W_2020.xlsx", FileFormat.XLSX);
            String excelFileName = String.format("pdf/out/%s.xlsx",FilenameUtils.removeExtension(pdfFile.getName()));
            pdfDocument.saveToFile(excelFileName, FileFormat.XLSX);

            pdfDocument.close();

            logger.info("Convert PDF to Excel Done : {}", excelFileName);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    private static void changeFont(){
        try{
            logger.info("_Run changeFont_");
            Locale newLocale = Locale.ROOT;
            Locale.setDefault(newLocale);

            ArrayList<File> pdfFileList = (ArrayList<File>) displayDirectoryContents(new File("pdf"),PDF_EXT);
            if (pdfFileList.isEmpty()){
                logger.info("Aucun fichier pdf trouvé");
                return;
            }

            for (File pdfFile: pdfFileList) {
                logger.info("Traitement du fichier {}",pdfFile.getName());

                // Créer un objet PdfDocument
                com.spire.pdf.PdfDocument pdfDocument = new com.spire.pdf.PdfDocument();

                // Charger un exemple de fichier PDF
                pdfFile = new File("pdf/_2021.pdf");
                pdfDocument.loadFromFile(pdfFile.getAbsolutePath());

                // Utilisez l'attribut UsedFonts de la classe PdfDocument pour obtenir toutes les polices utilisées dans le document.
                PdfUsedFont[] fonts = pdfDocument.getUsedFonts();

                PdfFont newfont = new PdfFont(PdfFontFamily.Helvetica, 11f);
                for (PdfUsedFont font: fonts) {
                    font.replace(newfont); // replace font
                }
                // pdfDocument.saveToFile(pdfFile.getAbsolutePath());

            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    public static void _deletingBlankColumns() throws Exception{

        com.aspose.cells.Workbook workbook = new com.aspose.cells.Workbook("pdf/out/0028894  W_2020.xlsx");
        com.aspose.cells.WorksheetCollection sheets = workbook.getWorksheets();
        com.aspose.cells.Worksheet sheet = sheets.get(0);

        //System.out.println(sheet.getCells().get(11,2).getBorders().getCount());
        Cell c ;//= sheet.getCells().get(1,13);
        /*
        System.out.println(c.getValue()+"");
        System.out.println(c.getStyle(true).getBorders().getByBorderType(BorderType.LEFT_BORDER).getLineStyle()+"");
        System.out.println(c.getStyle(true).getBorders().getByBorderType(BorderType.RIGHT_BORDER).getLineStyle()+"");
        System.out.println(c.getStyle(true).getBorders().getByBorderType(BorderType.TOP_BORDER).getLineStyle()+"");
        System.out.println(c.getStyle(true).getBorders().getByBorderType(BorderType.BOTTOM_BORDER).getLineStyle()+"");
        */

        boolean start = false;
        int startI =-1,startJ=-1,
                style,r;
        for (int i = 0; i < sheet.getCells().getMaxColumn(); i++) {
            for (int j = 0; j < sheet.getCells().getMaxColumn(); j++) {
                c = sheet.getCells().get(i,j);
                if("REF".equalsIgnoreCase(c.getValue()+"")){
                    startI =i;
                    startJ =j;
                    break;
                }
            }
            if(startI>-1){
                break;
            }
        }

        List<Integer> columnIndex = new ArrayList<>();
        for (int j = startJ; j < sheet.getCells().getMaxColumn(); j++) {
            columnIndex.add(0);
        }

        for (int i = startI; i < sheet.getCells().getMaxRow(); i++){

            // System.out.println("cell_value ====> " + c.getValue());

            for (int j = startJ; j < sheet.getCells().getMaxColumn(); j++) {
                c = sheet.getCells().get(i,j);
                style = c.getStyle(true).getBorders().getByBorderType(BorderType.LEFT_BORDER).getLineStyle();
                r=Math.max(
                        columnIndex.get(j-startJ),
                        style
                );
                columnIndex.set(j-startJ,r);
                System.out.print(style+" ");
            }
            System.out.println();
        }
        for (int j:columnIndex ) {
            System.out.print(j+" ");
        }

        System.out.println();
        System.out.println();

        com.aspose.cells.Workbook workbookTo = new com.aspose.cells.Workbook();
        workbook.getWorksheets().add();
        Worksheet sheetsTo = workbookTo.getWorksheets().get(0);
        sheetsTo.getCells().insertRows(0,sheet.getCells().getMaxRow()-startI);
        sheetsTo.getCells().insertColumns(0,columnIndex.size());


        System.out.println("sheet.getCells().getMaxColumn() "+sheet.getCells().getMaxColumn());
        System.out.println("columnIndex "+columnIndex.size());
        System.out.println("startJ "+startJ);


        int currentCol;
        String val;
        for (int i = startI; i < sheet.getCells().getMaxRow(); i++) {
            currentCol = 0;
            val = "";
            for (int j = startJ; j < sheet.getCells().getMaxColumn(); j++) {
                if(j>0 && columnIndex.get(j-startJ)>0){
                    sheetsTo.getCells().get(i-startI,currentCol).setValue(val);
                    currentCol++;
                    val = "";
                }
                c = sheet.getCells().get(i,j);
                val = val+c.getStringValue();
                //System.out.print(c.getValue());
            }
            System.out.println();
        }
        workbookTo.save("pdf/traites/to.xlsX");



        /*
        // aspose : URL  https://blog.conholdate.com/2020/12/25/delete-blank-rows-and-columns-in-excel-using-csharp/
        com.aspose.cells.Workbook wb = new com.aspose.cells.Workbook("pdf/out/0815951 Y_2020.xlsx");

        com.aspose.cells.WorksheetCollection sheets1 = wb.getWorksheets();

        com.aspose.cells.Worksheet sheet1 = sheets1.get(0);

        // Cette option assurera les références (dans les formules, les graphiques)
        // sont mis à jour lors de la suppression des lignes et des colonnes vides.
        com.aspose.cells.DeleteOptions options = new com.aspose.cells.DeleteOptions();
        options.setUpdateReference(true);

        // Supprimez les lignes et les colonnes vides.
        sheet1.getCells().deleteBlankColumns(options);
        sheet1.getCells().deleteBlankColumns(options);

        // Calculer les formules du classeur
        // wb.calculateFormula();

        wb.save("pdf/traites/0815951 Y_2020_.xlsx");
		*/
    }

    public static List<File> displayDirectoryContents(File fileDirectory,String extention) {
        ArrayList<File> pdfFileList = new ArrayList<>();
        File[] files = fileDirectory.listFiles();
        for (File file:files){
            if (file.isDirectory()){
                file.delete();
                continue;
            }
            if (file.getName().endsWith((String.format(".%s",extention)))){
                pdfFileList.add(file);
            }

            /*
            if (file.isDirectory()){
                pdfFileList.addAll(displayDirectoryContents(file,PDF_EXT));
            } else {
                if (file.getName().endsWith((String.format(".%s",ext)))){
                    pdfFileList.add(file);
                }
            }
            */
        }

        return pdfFileList;
    }
}