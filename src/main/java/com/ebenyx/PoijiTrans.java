package com.ebenyx;
import com.ebenyx.entities.TransactionAutorise;
import com.ebenyx.jpa.TransactionAutoriseRepository;
import com.google.gson.Gson;
import com.poiji.bind.Poiji;
import com.poiji.option.PoijiOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * @author desireadje
 * @project ocr_app
 * @created 24/08/2022 - 13:15
 */
public class PoijiTrans {
    private static final Logger logger = LoggerFactory.getLogger(PoijiTrans.class);

    public void main(TransactionAutoriseRepository repository) throws FileNotFoundException {
        long startTime = new Date().getTime();
        Gson gson = new Gson();

        // _Poiji_Trans_
        File file = new File("Poiji_Trans/File_test.xlsx");
        if (!file.exists()){
            logger.info("File not existe {}",file.getAbsolutePath());
            return;
        }

        PoijiOptions poijiOptions = PoijiOptions.PoijiOptionsBuilder
                                                .settings()
                                                .sheetIndex(1)
                                                .build();

        InputStream stream = new FileInputStream(file);
        ArrayList<TransactionAutorise> autoriseList = (ArrayList) Poiji.fromExcel(file,TransactionAutorise.class);
        // List<TransactionAutorise> autoriseList = Poiji.fromExcel(stream,PoijiExcelType.XLSX,TransactionAutorise.class,poijiOptions);
        // List<TransactionAutorise> autoriseList = Poiji.fromExcel(file,TransactionAutorise.class,options);

        System.out.println("test : "+autoriseList.size());
        System.out.println("==> " + new Gson().toJson(autoriseList.get(0)));

        for (TransactionAutorise autorise:autoriseList) {
            logger.info("Save line {}",autorise.getRowIndex());
            repository.save(autorise);
        }

        long endTime = new Date().getTime();
        long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(endTime-startTime);

        System.out.println(String.format("%s ligne(s) traitées en %s seconde(s), %s minute(s)",
                                            autoriseList.size(),
                                            diffInSeconds,
                                            diffInSeconds/60));
    }

    public void listener(){
        // _Poiji_Trans_
        File file = new File("Poiji_Trans/File_test.xlsx");
        if (!file.exists()){
            logger.info("File not existe {}",file.getAbsolutePath());
            return;
        }

        Poiji.fromExcel(file,TransactionAutorise.class, new Consumer<TransactionAutorise>(){
            @Override
            public void accept(TransactionAutorise t) {
                System.out.println("getRowIndex ==> "+t.getRowIndex());
            }
        });
    }
}
