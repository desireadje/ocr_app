package com.ebenyx.controller.rest;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

/**
 * @author desireadje
 * @created 03/10/2022 - 10:05
 * @project ocr_app
 */
@org.springframework.web.bind.annotation.RestController
@CrossOrigin("*")
@RequestMapping(value = "/oracle/api")
public class RestController {

    @RequestMapping(
            value = "/init_stage_interface/",
            method = RequestMethod.POST,
            produces = { org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE })
    public Object init_stage_interface(@RequestParam MultiValueMap<String,String> requestBobdy) {
        try {
            if (requestBobdy.getFirst("xml_data")==null || requestBobdy.getFirst("async")==null){
                return new ResponseEntity<>("Incorrect or missing parameters",HttpStatus.BAD_REQUEST);
            }

            okhttp3.OkHttpClient client = new okhttp3.OkHttpClient.Builder()
                    .addInterceptor(new BasicAuthInterceptor("IntegrationUser","Oracle2022"))
                    .build();

            okhttp3.MediaType mediaType = okhttp3.MediaType.parse("application/x-www-form-urlencoded");
            okhttp3.RequestBody body = okhttp3.RequestBody.create(mediaType,
                    String.format("xml_data=%s&async=%s",requestBobdy.getFirst("xml_data"),requestBobdy.getFirst("async")));
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url("https://tc4.wms.ocs.oraclecloud.com:443/orange_test/wms/api/init_stage_interface/")
                    .post(body)
                    .addHeader("content-type","application/x-www-form-urlencoded")
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            if (!(response.code()==HttpStatus.OK.value())) {
                getLogger().info("code ====> " + response.code());
                return new ResponseEntity<>(HttpStatus.valueOf(response.code()));
            }

            String responseBodyAsString = response.body().string();
            getLogger().info("responseBodyAsString ====> " + responseBodyAsString);

            JSONObject payloadJsonObject = XML.toJSONObject(responseBodyAsString);
            getLogger().info("payloadJsonObject ====> " + payloadJsonObject);

            JSONObject rootJSONObject = new JSONObject();
            Boolean successJSONObject = Boolean.FALSE;

            rootJSONObject = (JSONObject) payloadJsonObject.get("root");
            successJSONObject = rootJSONObject.getBoolean("success");

            if (!successJSONObject){
                return rootJSONObject;
            }

            // on va vefieir s'il existe







        } catch (Exception e) {
            e.printStackTrace();
            getLogger().error(e.getMessage());
            return null;
        }

        return "okkkoko";
    }

    public Logger getLogger(){
        return LoggerFactory.getLogger(RestController.class);
    }

}
