package com.ebenyx.job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 *
 * @URL : https://www.e-iceblue.com/Tutorials/Java/Spire.PDF-for-Java/Program-Guide/Conversion/Convert-PDF-to-Excel-in-Java.html
 *  * @URL : https://www.e-iceblue.com/Tutorials/Java/Spire.PDF-for-Java/Program-Guide/Conversion/Java-Convert-a-Multi-page-PDF-to-One-Excel-Worksheet.html
 *  * @URL (Gestion de la police) : https://stackoverflow.com/questions/68608157/how-can-i-fix-this-warning-the-fonts-times-and-times-are-not-available-fo
 *
 * @author desireadje
 * @project ocr_app
 * @created 17/08/2022 - 10:05
 */
@Service
public class Job {
    private static final Logger logger = LoggerFactory.getLogger(Job.class);

    private static String PDF_EXT = "pdf";
    private static String XLSX_EXT = "xlsx";

    final static String INPUT_FOLDER = "OCR-App/Repertoire_depot/fichiers_output";              // dossiers de recperation
    final static String OUTPUT_FOLDER = "OCR-App/Repertoire_depot/ready_for_camelot";           // dossiers de restituration
    final static String TMP_FOLDER = "OCR-App/Repertoire_depot/tmp";                            // dossiers tempon

    @Async
    public void readPdf(){
        try{
            createFolder(INPUT_FOLDER);
            createFolder(OUTPUT_FOLDER);
            createFolder(TMP_FOLDER);

            // supprimer tous les fichiers et sous-répertoires
            org.apache.commons.io.FileUtils.cleanDirectory(new java.io.File(TMP_FOLDER));

            Locale newLocale = Locale.ROOT;
            Locale.setDefault(newLocale);

            // recuperation des fichiers (.pdf)
            ArrayList<java.io.File> pdfFileList = (ArrayList<java.io.File>) displayDirectoryContents(new java.io.File(INPUT_FOLDER),PDF_EXT);
            if (pdfFileList.isEmpty()){
                logger.info("Aucun fichier pdf trouvé");
                return;
            }

            for (java.io.File pdfFile: pdfFileList) {
                logger.info("Traitement du fichier {}",pdfFile.getName());

                // Créer un objet PdfDocument
                com.spire.pdf.PdfDocument pdfDocument = new com.spire.pdf.PdfDocument();

                // Charger un exemple de fichier PDF
                pdfDocument.loadFromFile(pdfFile.getAbsolutePath());
                // pdfDocument.loadFromFile("pdf/0028894  W_2020.pdf");

                // Définir les options de conversion PDF vers XLSX : rendu de plusieurs pages sur une seule feuille de calcul
                pdfDocument.getConvertOptions().setPdfToXlsxOptions(new com.spire.pdf.conversion.XlsxLineLayoutOptions(true,true,true));

                // Enregistrer dans Excel
                // pdfDocument.saveToFile("pdf/traites/0028894  W_2020.xlsx", FileFormat.XLSX);
                String excelFileName = String.format("%s/%s.xlsx",TMP_FOLDER,org.apache.commons.io.FilenameUtils.removeExtension(pdfFile.getName()));
                pdfDocument.saveToFile(excelFileName, com.spire.pdf.FileFormat.XLSX);

                pdfDocument.close();

                logger.info("Convert PDF to Excel Done : {}", excelFileName);

                java.nio.file.Files.deleteIfExists(java.nio.file.Paths.get(pdfFile.getAbsolutePath()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }
    }

    @Async
    public void excelTransformation() throws Exception{
        // recuperation des fichiers du dossier tempon
        ArrayList<java.io.File> tmpPdfList = (ArrayList<java.io.File>) displayDirectoryContents(new java.io.File(TMP_FOLDER),XLSX_EXT);
        if (tmpPdfList.isEmpty()){
            logger.info("Aucun fichier pdf trouvé");
            return;
        }

        for (java.io.File tmpPdf: tmpPdfList){
            logger.info("Traitement du fichier {}",tmpPdf.getName());

            com.aspose.cells.Workbook workbook = new com.aspose.cells.Workbook(tmpPdf.getAbsolutePath());
            com.aspose.cells.WorksheetCollection sheets = workbook.getWorksheets();

            com.aspose.cells.Worksheet sheet0 = sheets.get(0);
            com.aspose.cells.Worksheet sheet1 = sheets.get(1);

            // suppression des colonne...
            deletingBlankColumns(workbook, sheet0, sheet1, tmpPdf);

            java.nio.file.Files.deleteIfExists(java.nio.file.Paths.get(tmpPdf.getAbsolutePath()));
        }

        /*

        File pdfFile = new File("pdf/traites/0028894  W_2020.xlsx");
        com.aspose.cells.Workbook workbook = new com.aspose.cells.Workbook(pdfFile.getAbsolutePath());
        com.aspose.cells.WorksheetCollection sheets = workbook.getWorksheets();
        com.aspose.cells.Worksheet sheet0 = sheets.get(0);
        com.aspose.cells.Worksheet sheet1 = sheets.get(1);
        _deletingBlankColumns(workbook, sheet0, sheet1, pdfFile);

        */
    }

    public static void deletingBlankColumns(com.aspose.cells.Workbook workbook, com.aspose.cells.Worksheet sheet0,
                                             com.aspose.cells.Worksheet sheet1, java.io.File tmpPdf) throws Exception{
        com.aspose.cells.Workbook workbookTo = new com.aspose.cells.Workbook();

        com.aspose.cells.Cell c = null;

        boolean start = false;
        int startI = -1, startJ = -1,style,r;
        for (int i = 0; i < sheet0.getCells().getMaxColumn(); i++) {
            for (int j = 0; j < sheet0.getCells().getMaxColumn(); j++) {
                c = sheet0.getCells().get(i,j);
                if("REF".equalsIgnoreCase((c.getValue()+"").trim()) || "Réf.".equalsIgnoreCase((c.getValue()+"").trim())){
                    startI =i;
                    startJ =j;
                    break;
                }
            }
            if(startI>-1){
                break;
            }
        }

        List<Integer> columnIndex = new ArrayList<>();
        for (int j = startJ; j < sheet0.getCells().getMaxColumn(); j++){
            columnIndex.add(0);
        }

        for (int i = startI; i < sheet0.getCells().getMaxRow(); i++){
            for (int j = startJ; j < sheet0.getCells().getMaxColumn(); j++){
                System.out.println("row "+i);
                c = sheet0.getCells().get(i,j);
                style = c.getStyle(true).getBorders().getByBorderType(com.aspose.cells.BorderType.LEFT_BORDER).getLineStyle();
                r=Math.max(
                        columnIndex.get(j-startJ),
                        style
                );
                columnIndex.set(j-startJ,r);
                System.out.print(style+" ");
            }
            System.out.println();
        }

        for (int j:columnIndex ) {
            System.out.print(j+" ");
        }


        // workbook.getWorksheets().add();
        // Worksheet sheetsTo = workbookTo.getWorksheets().get(0);
        com.aspose.cells.Worksheet sheetsTo = workbookTo.getWorksheets().insert(0, com.aspose.cells.SheetType.CHART, "sheet 1");
        sheetsTo.getCells().insertRows(0,sheet0.getCells().getMaxRow()-startI);
        sheetsTo.getCells().insertColumns(0,columnIndex.size());


        //System.out.println("sheet.getCells().getMaxColumn() "+sheet.getCells().getMaxColumn());
        //System.out.println("columnIndex "+columnIndex.size());
        //System.out.println("startJ "+startJ);


        int currentCol;
        String val;
        for (int i = startI; i < sheet0.getCells().getMaxRow(); i++){
            currentCol = 0;
            val = "";
            for (int j = startJ; j < sheet0.getCells().getMaxColumn(); j++){
                if(j>0 && columnIndex.get(j-startJ)>0){
                    sheetsTo.getCells().get(i-startI,currentCol).setValue(val);
                    currentCol++;
                    val = "";
                }
                c = sheet0.getCells().get(i,j);
                val = val+c.getStringValue();
                //System.out.print(c.getValue());
            }
            System.out.println();
        }











        ///////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////
        com.aspose.cells.Cell _c = null;

        boolean _start = false;
        int _startI = -1, _startJ = -1,_style,_r;
        for (int i = 0; i < sheet1.getCells().getMaxColumn(); i++) {
            for (int j = 0; j < sheet1.getCells().getMaxColumn(); j++) {
                _c = sheet1.getCells().get(i,j);
                if("REF".equalsIgnoreCase((_c.getValue()+"").trim()) || "Réf.".equalsIgnoreCase((_c.getValue()+"").trim())){
                    _startI =i;
                    _startJ =j;
                    break;
                }
            }
            if(_startI>-1){
                break;
            }
        }

        List<Integer> _columnIndex = new ArrayList<>();
        for (int j = _startJ; j < sheet1.getCells().getMaxColumn(); j++){
            _columnIndex.add(0);
        }

        for (int i = _startI; i < sheet1.getCells().getMaxRow(); i++){
            for (int j = _startJ; j < sheet1.getCells().getMaxColumn(); j++){
                System.out.println("row "+i);
                _c = sheet1.getCells().get(i,j);
                _style = _c.getStyle(true).getBorders().getByBorderType(com.aspose.cells.BorderType.LEFT_BORDER).getLineStyle();
                _r=Math.max(
                        _columnIndex.get(j-_startJ),
                        _style
                );
                _columnIndex.set(j-_startJ,_r);
                System.out.print(_style+" ");
            }
            System.out.println();
        }

        for (int j:_columnIndex ) {
            System.out.print(j+" ");
        }


        // workbook.getWorksheets().add();
        // Worksheet _sheetsTo = workbookTo.getWorksheets().get(1);
        com.aspose.cells.Worksheet _sheetsTo = workbookTo.getWorksheets().insert(1, com.aspose.cells.SheetType.CHART, "sheet 2");
        _sheetsTo.getCells().insertRows(0,sheet1.getCells().getMaxRow()-_startI);
        _sheetsTo.getCells().insertColumns(0,_columnIndex.size());


        //System.out.println("sheet.getCells().getMaxColumn() "+sheet.getCells().getMaxColumn());
        //System.out.println("columnIndex "+columnIndex.size());
        //System.out.println("startJ "+startJ);


        int _currentCol;
        String _val;
        for (int i = _startI; i < sheet1.getCells().getMaxRow(); i++){
            _currentCol = 0;
            _val = "";
            for (int j = _startJ; j < sheet1.getCells().getMaxColumn(); j++){
                if(j>0 && _columnIndex.get(j-_startJ)>0){
                    _sheetsTo.getCells().get(i-_startI,_currentCol).setValue(_val);
                    _currentCol++;
                    _val = "";
                }
                _c = sheet1.getCells().get(i,j);
                _val = _val+_c.getStringValue();
            }
            System.out.println();
        }


        String excelFileName = String.format("%s/%s.xlsx",OUTPUT_FOLDER,org.apache.commons.io.FilenameUtils.removeExtension(tmpPdf.getName()));
        workbookTo.save(excelFileName);

        logger.info("Deleting Blank Columns Done : {}", excelFileName);
    }




    public static void createFolder(String pathString){
        java.io.File dir = new java.io.File(pathString);
        if (!dir.exists()){
            dir.mkdirs();
            logger.info("Path created {} ",java.nio.file.Paths.get(pathString));
        }
    }

    public static List<java.io.File> displayDirectoryContents(java.io.File fileDirectory, String extention){
        ArrayList<java.io.File> pdfFileList = new ArrayList<>();
        java.io.File[] files = fileDirectory.listFiles();
        for (java.io.File file:files){
            if (file.isDirectory()){
                file.delete();
                continue;
            }
            if (file.getName().endsWith((String.format(".%s",extention)))){
                pdfFileList.add(file);
            }
        }
        return pdfFileList;
    }
}
