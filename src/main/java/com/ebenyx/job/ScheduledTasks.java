package com.ebenyx.job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;



/**
 * @author desireadje
 * @project ocr_app
 * @created 17/08/2022 - 10:03
 */
@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final long SECONDE = 1000;
    private static final long MINUTE = SECONDE * 60;
    private static final long HEURE = MINUTE * 60;

    @Autowired private transient Job job;

    @Scheduled(fixedDelay = 20 * SECONDE)
    public void ReadPdfTask() {
        logger.info("[_Run ReadPdfTask_]");
        job.readPdf();
    }

    @Scheduled(fixedDelay = 10 * SECONDE)
    public void ExcelTransformationTask() throws Exception {
        logger.info("[_Run ExcelTransformationTask_]");
        job.excelTransformation();
    }
}
