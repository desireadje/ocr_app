package com.ebenyx.entities;
import com.poiji.annotation.ExcelCellName;
import com.poiji.annotation.ExcelRow;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * @author desireadje
 * @project ocr_app
 * @created 24/08/2022 - 11:23
 */
@Getter
@Setter

@Entity
@Table(name = "transaction_autorise")
public class TransactionAutorise {
    private static final long serialVersionUID = 1L;

    @ExcelCellName("id")
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    @ExcelRow
    private int rowIndex;

    public String messageType;

    @ExcelCellName("referenceNumber")
    public String referenceNumber;

    @ExcelCellName("authorizationCode")
    public String authorizationCode;

    @ExcelCellName("internalTransmissionTime")
    @Temporal(TemporalType.TIMESTAMP)
    public Date internalTransmissionTime;

    @ExcelCellName("sourceCode")
    public String sourceCode;

    @ExcelCellName("destination")
    public String destination;

    @ExcelCellName("emetteur")
    public String emetteur;

    @ExcelCellName("acquereur")
    public String acquereur;

    @ExcelCellName("processing")
    public String processing;

    @ExcelCellName("actionCode")
    public String actionCode;

    @ExcelCellName("authSource")
    public String authSource;

    @ExcelCellName("reponseEmetteur")
    public String reponseEmetteur;

    @ExcelCellName("transactionAmount")
    public Double transactionAmount;

    @ExcelCellName("cardAcceptorTermId")
    public String cardAcceptorTermId;

    @ExcelCellName("evenement")
    public String evenement;

    @ExcelCellName("reversalFlag")
    public String reversalFlag;

    @ExcelCellName("endRxpiryDate")
    @Temporal(TemporalType.DATE)
    public Date endRxpiryDate;

    @ExcelCellName("fraudCode")
    public String fraudCode;

    @ExcelCellName("fraudLabel")
    public String fraudLabel;

    @ExcelCellName("transactionFee")
    public Double transactionFee;

    @ExcelCellName("sourceAccountNumber")
    public String sourceAccountNumber;

    @ExcelCellName("dateChargement")
    @Temporal(TemporalType.TIMESTAMP)
    public Date dateChargement;

    @ExcelCellName("comptabilise")
    public boolean comptabilise;

    @ExcelCellName("telecollecte")
    public Boolean telecollecte;

    @ExcelCellName("businessDate")
    @Temporal(TemporalType.DATE)
    public Date businessDate;

    @ExcelCellName("couverte")
    public boolean couverte;

    @ExcelCellName("reservationDeFondExpire")
    public Boolean reservationDeFondExpire;

    // cle journal_dab_id

    public TransactionAutorise() {
    }
}
