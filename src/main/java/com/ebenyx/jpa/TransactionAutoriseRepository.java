package com.ebenyx.jpa;
import com.ebenyx.entities.TransactionAutorise;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author desireadje
 * @project ocr_app
 * @created 24/08/2022 - 16:15
 */
public interface TransactionAutoriseRepository extends JpaRepository<TransactionAutorise,Long>{
}
